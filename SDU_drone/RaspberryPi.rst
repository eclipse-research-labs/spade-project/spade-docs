.. toctree::
    :glob:

.. _raspberry_Pi:

Interacting with the Raspberry Pi
#################################

.. image:: images/Raspberry_Pi.png
    :width: 400px
    :align: center
    :alt: Raspberry


Flashing the Raspberry Pi
=============================
If you have purchased the preassembled drone then your SD card should already be flashed with the correct image. If you decide to create your own drone or you have problems with the SD card, then you can get the recommended image from `here <https://github.com/Markus-Simonsen/Spade_docs/>`_. Then you can flash any SD card by using for example Win32 Disk Imager.


SSH into the Raspberry Pi
=============================

To interact with the microcontroller on the drone, and run programs on it, one good option is to use SSH. This enables you to easily contact the Raspberry Pi without the need for a USB cable. For this, you will need to have your computer and Raspberry Pi running on the same network. When you have a common internet connection. You need the IP address of the Raspberry Pi. This can be found by running the following command in the Raspberry Pi terminal:

.. note:: To run commands in the terminal of the Raspberry Pi, you can connect a monitor, mouse and keyboard to it.
.. note:: Be aware that just connecting your computer and Raspberry Pi directly with an ethernet cable can be troublesome, since there is no router to give out IP addresses.

.. code-block:: console

    ifconfig

To check that your computer is connected to the Raspberry Pi you can ping it from your computer's terminal.

.. code-block:: console

    ping microcontroller-IP

Where microcontroller-IP is replaced with the IP that you found in your Raspberry Pi. If you get a response from the Raspberry Pi, you are ready to SSH into it.


The username and password were chosen when you flashed the image onto the microcontroller.
The command to SSH into the Raspberry Pi is:

.. code-block:: console

    ssh username@microcontroller-IP

Where username is exchanged with the username of your microcontroller and microcontroller-IP is replaced with the IP that you found. After running this command the terminal will ask for the password of the microcontroller. After entering the password you will be logged into the microcontroller.

.. note:: 
    Be aware that while writing the password into the terminal, it will not show any characters, this is normal.


Next Steps 
==============
The next session will explain how to simulate the drone, and how to load your program onto the drone to make it fly autonomous missions.