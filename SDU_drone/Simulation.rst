.. toctree::
    :glob:

.. _simulation:

ROS2 node in simulation and reality
######################################
In this section, we will take a look at trying to run a simple ROS2 node both in simulation and on your actual drone.
.. note:: Be aware that this is not a ROS2 tutorial, if you want to learn more about using ROS2 please refer to their `guide <https://docs.ros.org/en/humble/index.html>`_

Simulation
=================
.. PX4 provides two different ways to simulate your drone. Either through hardware in the loop (HIL), or through software in the loop (SIL). If you choose to use HIL then the robot's hardware is put into a simulation, so all of the peripherals (sensors, actuators, etc) will be simulated. On the contrary, if you use SIL then everything will be run in the simulation. The advantage of using HIL is that it is closer to reality and will therefore most likely identify more bugs. However, SIL has the benefit of not needing a drone to do simulations. We will first go through SIL, whereafter we will look at SIL.

To set up ROS2 with your PX4 flight controller and get started with simulation, please refer to this `guide <https://docs.px4.io/main/en/ros/ros2_comm.html>`_ and follow it at least until reaching the examples. When downloading the PX4 software, we strongly recommend, that you download one of their releases from their GitHub. Otherwise, you are at risk of encountering new bugs.

After completing their guide, you can then download our ROS2node from `here <https://github.com/Markus-Simonsen/Spade_docs/>`_ and run it in your ROS2 environment.



Real-world
============
To run the ROS2 node on your actual drone the first step is to enter QGroundControl then go to Analyze Tools then MAVLink Console and type `ver all`. Under the tag "HW type:" you should now see the board name of the drone. Going into the PX4 directory that you downloaded in the previous section find the following file: PX4-autopilot/src/modules/uxrce_dds_client/dds_topics.yaml. Here you can then see all of the topics that the flight controller will publish, and uncomment the ones that you will need.

Before trying to compile the code please check that you have the right compiler installed by running the command below.

.. code-block:: console
    arm-none-eabi-gcc --version

Next, run the following file and command from the root of your PX4-Autopilot directory. Make sure to replace <board-model> with your board model.

.. code-block:: console

    Tools/setup/ubuntu.sh
    make <board-model>

Now just flash your PX4 firmware through QGroundControl and your flight controller is ready. When you arm your drone and switch over to offboard flight mode, your drone should start executing the flight specified in the ROS node. At this point you should be ready to modify the ROS node and begin making your programs to execute pre-determined flights
