.. toctree::
    :glob:


Hardware components
#########################

Preassembled drone
==================

.. image:: images/Assembled_drone.jpg
    :width: 400px
    :align: center
    :alt: Drone

If you bought the preassembled drone, you will still need to buy the following components to follow along with the tutorial.

- Battery
- Charger


.. note:: The drone can be bought at this `link/need to be changed!!!! <https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjW_MqEyfaDAxVmcvEDHTaxCDEQ3yx6BAgnEAI&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DdQw4w9WgXcQ&usg=AOvVaw0aHtehaphMhOCAkCydRLZU&opi=89978449>`_.



Custom Drone
==================

If you did not buy the preassembled drone, you will in addition to the abovementioned components also need to buy the necessary components to build the drone.
The drone that this tutorial is based on uses the following components, so you can choose to buy those or other components of your own choice.

- Pixhawk 6c mini flight controller
- PM06 v2 micro power module
- Carbon fiber 250 airframe with hardware
- motors 2207 KV 1950
- 5" Plastic props
- M10 GPS
- Fully assembled Power Management Board with ESCs(BLHeli S ESC 20A)
- 433MHz Telemetry Radio/ 915MHz Telemetry Radio
- 5.8G FPV Video Transmitter
- Micro OSD V2
- Foxeer Predator 5 Micro FPV Camera
- Power and Radio Cables
- Battery Straps
- Battery observer
- Remote Controller (we recommend Turnigy Evolution Pro)
- Telemetry module

3D prints
=====================
You can take a look at the 3D-prints in this `repo <https://github.com/Markus-Simonsen/Spade_docs/>`_, these will follow along with the preassembled drone. If they break, or if you just want to build a drone from scratch, then you can just 3D-print them and attach them yourself.

.. error:: insert picture of the relevant 3D-prints and where they fit


Next Steps
==================
Next, now that the drone is assembled and ready. Let's get the battery ready!

