.. Spade documentation master file, created by
   sphinx-quickstart on Sun Jan 21 21:05:33 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Spade's documentation!
#################################

If you already know how to operate drones, you can just jump directly to :ref:`raspberry_pi` section .

.. toctree::
   :maxdepth: 2
   :caption: Contents: 
   :numbered:
   :glob:

   Hardware.rst
   Battery.rst
   Flying.rst
   RaspberryPi.rst
   Simulation.rst



..    Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
