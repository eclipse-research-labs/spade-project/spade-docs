
Welcome to Spade’s documentation![](#welcome-to-spade-s-documentation "Link to this heading")
==============================================================================================

If you already know how to operate drones, you can just jump directly to [Interacting with the Raspberry Pi](RaspberryPi.md#raspberry-pi) section .

Contents:

*   [1\. Hardware components](src/hardware.md)
    *   [1.1. Preassembled drone](src/hardware.md#preassembled-drone)
    *   [1.2. Custom Drone](src/hardware.md#custom-drone)
    *   [1.3. 3D prints](src/hardware.md#d-prints)
    *   [1.4. Next Steps](src/hardwaremd#next-steps)
*   [2\. Charging the battery](src/battery.md)
    *   [2.1. Connecting the battery and charger](src/battery.md#connecting-the-battery-and-charger)
    *   [2.2. Setup the Charger](src/battery.md#setup-the-charger)
    *   [2.3. Connecting the parts](src/battery.md#connecting-the-parts)
    *   [2.4. Next Steps](src/battery.md#next-steps)
*   [3\. Flying](src/Flying.md)
    *   [3.1. Requirements](src/flying.md#requirements)
    *   [3.2. Connecting to the drone](src/flying.md#connecting-to-the-drone)
    *   [3.3. Setting up the drone](src/flying.md#setting-up-the-drone)
    *   [3.4. Setting up the RC](src/flying.md#setting-up-the-rc)
    *   [3.5. Flying](src/flying.md#id1)
    *   [3.6. Next steps](src/flying.md#next-steps)
*   [4\. Interacting with the Raspberry Pi](src/raspberryPi.md)
    *   [4.1. Flashing the Raspberry Pi](src/raspberryPi.md#flashing-the-raspberry-pi)
    *   [4.2. SSH into the Raspberry Pi](src/raspberryPi.md#ssh-into-the-raspberry-pi)
    *   [4.3. Next Steps](src/raspberryPi.md#next-steps)
*   [5\. ROS2 node in simulation and reality](src/simulation.md)
    *   [5.1. Simulation](src/simulation.md#id1)
    *   [5.2. Real-world](src/simulation.md#real-world)

[Next](src/hardware.md "1. Hardware components")

* * *

