
4\. Interacting with the Raspberry Pi[](#interacting-with-the-raspberry-pi "Link to this heading")
===================================================================================================

[![Raspberry](../images/Raspberry_Pi.png)](../images/Raspberry_Pi.png)

4.1. Flashing the Raspberry Pi[](#flashing-the-raspberry-pi "Link to this heading")
------------------------------------------------------------------------------------

If you have purchased the preassembled drone then your SD card should already be flashed with the correct image. If you decide to create your own drone or you have problems with the SD card, then you can get the recommended image from [here](https://github.com/Markus-Simonsen/Spade_docs/). Then you can flash any SD card by using for example Win32 Disk Imager.

4.2. SSH into the Raspberry Pi[](#ssh-into-the-raspberry-pi "Link to this heading")
------------------------------------------------------------------------------------

To interact with the microcontroller on the drone, and run programs on it, one good option is to use SSH. This enables you to easily contact the Raspberry Pi without the need for a USB cable. For this, you will need to have your computer and Raspberry Pi running on the same network. When you have a common internet connection. You need the IP address of the Raspberry Pi. This can be found by running the following command in the Raspberry Pi terminal:

Note

To run commands in the terminal of the Raspberry Pi, you can connect a monitor, mouse and keyboard to it.

Note

Be aware that just connecting your computer and Raspberry Pi directly with an ethernet cable can be troublesome, since there is no router to give out IP addresses.

ifconfig

To check that your computer is connected to the Raspberry Pi you can ping it from your computer’s terminal.

ping microcontroller-IP

Where microcontroller-IP is replaced with the IP that you found in your Raspberry Pi. If you get a response from the Raspberry Pi, you are ready to SSH into it.

The username and password were chosen when you flashed the image onto the microcontroller. The command to SSH into the Raspberry Pi is:

ssh username@microcontroller-IP

Where username is exchanged with the username of your microcontroller and microcontroller-IP is replaced with the IP that you found. After running this command the terminal will ask for the password of the microcontroller. After entering the password you will be logged into the microcontroller.

Note

Be aware that while writing the password into the terminal, it will not show any characters, this is normal.

4.3. Next Steps[](#next-steps "Link to this heading")
------------------------------------------------------

The next session will explain how to simulate the drone, and how to load your program onto the drone to make it fly autonomous missions.

[Previous](/Flying.md "3. Flying") [Next](/Simulation.md "5. ROS2 node in simulation and reality")

* * *

© Copyright 2024, Markus Simonsen, SDU.