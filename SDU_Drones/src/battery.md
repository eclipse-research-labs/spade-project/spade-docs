2\. Charging the battery[](#charging-the-battery "Link to this heading")
=========================================================================

2.1. Connecting the battery and charger[](#connecting-the-battery-and-charger "Link to this heading")
------------------------------------------------------------------------------------------------------

To charge the batteries with the charger first connect the battery and charger. The outermost black wire on the battery should be connected at the minus sign on the charger, the following wires indicate the different cells on the battery.

2.2. Setup the Charger[](#setup-the-charger "Link to this heading")
--------------------------------------------------------------------

No matter which charger you are using, there are a couple of settings that need to be set up. We will now go through each of these and how to set them up.

### 2.2.1. Select task[](#select-task "Link to this heading")

The charger typically has two modes: Charge and storage The charge function is used to charge your batteries and prepare them for use. The storage function is used when you want to store your batteries for an extended period of time and will prolong the lifetime of your batteries

### 2.2.2. Battery type[](#battery-type "Link to this heading")

The battery type can be found on your battery or with a quick Google search, however, commonly drones will use LiPo batteries.

### 2.2.3. Cell voltage[](#cell-voltage "Link to this heading")

The cell voltage can be seen in the table below. The optimal fields to be in here are the grey ones. This means that you should be careful not to charge your batteries too high, and also not to discharge them to a too low level. Discharging your batteries too low can be avoided by using the battery alarm as explained later.

[![_images/lipotable.png](../images/lipotable.png)](_images/lipotable.png)

### 2.2.4. Cell count[](#cell-count "Link to this heading")

The cell count can also be found on your battery, it is indicated by a number followed by an S.

### 2.2.5. Current setting[](#current-setting "Link to this heading")

The current setting can be calculated from the battery’s c-charge rating. It is important to note that the C-charge rating and C-discharge rating are not the same. To be safe you can always use a C-charge rating of 1. The formula for calculating the charge current can be seen below.

\\\[I\_{charge} = C\_{charge} \* Capacity\\\]

2.3. Connecting the parts[](#connecting-the-parts "Link to this heading")
--------------------------------------------------------------------------

After having safely charged the battery, the next step is to connect it to the drone and mount it on top as seen in the picture below.

[![_images/Assembled_drone.jpg](../images/Assembled_drone.jpg)](_images/Assembled_drone.jpg)

Additionally, it is also important to connect the battery alarm. This is connected as seen in the picture below.

[![_images/Battery_monitor.jpg](../images/Battery_monitor.jpg)](_images/Battery_monitor.jpg)

Note

If the battery is a little loose, it can be recommended to use strips to secure it to the drone.

When properly connected the observer will beep and start showing the battery levels of the cells. The purpose of the battery observer is to alarm you before the battery cells reach a critically low level, both to avoid damaging the batteries and to avoid crashing. It does this by starting to beep at you when the battery level is too low. At this point, you should start trying to land the drone. When it starts beeping again, the drone will soon crash and you should stay clear.

2.4. Next Steps[](#next-steps "Link to this heading")
------------------------------------------------------

Now that the battery is ready, let’s get ready to fly the drone!

[Previous](/hardware.md "1. Hardware components") [Next](Flying.md "3. Flying")

* * *

© Copyright 2024, Markus Simonsen, SDU.
