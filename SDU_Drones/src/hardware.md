

1\. Hardware components[](#hardware-components "Link to this heading")
=======================================================================

1.1. Preassembled drone[](#preassembled-drone "Link to this heading")
----------------------------------------------------------------------

[![Drone](../images/Assembled_drone.jpg)](../images/Assembled_drone.jpg)

If you bought the preassembled drone, you will still need to buy the following components to follow along with the tutorial.

*   Battery
    
*   Charger
    

1.2. Custom Drone[](#custom-drone "Link to this heading")
----------------------------------------------------------

If you did not buy the preassembled drone, you will in addition to the abovementioned components also need to buy the necessary components to build the drone. The drone that this tutorial is based on uses the following components, so you can choose to buy those or other components of your own choice.

*   Pixhawk 6c mini flight controller
    
*   PM06 v2 micro power module
    
*   Carbon fiber 250 airframe with hardware
    
*   motors 2207 KV 1950
    
*   5” Plastic props
    
*   M10 GPS
    
*   Fully assembled Power Management Board with ESCs(BLHeli S ESC 20A)
    
*   433MHz Telemetry Radio/ 915MHz Telemetry Radio
    
*   5.8G FPV Video Transmitter
    
*   Micro OSD V2
    
*   Foxeer Predator 5 Micro FPV Camera
    
*   Power and Radio Cables
    
*   Battery Straps
    
*   Battery observer
    
*   Remote Controller (we recommend Turnigy Evolution Pro)
    
*   Telemetry module
    

1.3. 3D prints[](#d-prints "Link to this heading")
---------------------------------------------------

You can take a look at the 3D-prints in this [repo](https://github.com/Markus-Simonsen/Spade_docs/), these will follow along with the preassembled drone. If they break, or if you just want to build a drone from scratch, then you can just 3D-print them and attach them yourself.

Error

insert picture of the relevant 3D-prints and where they fit

1.4. Next Steps[](#next-steps "Link to this heading")
------------------------------------------------------

Next, now that the drone is assembled and ready. Let’s get the battery ready!

[Previous](../Readme.md "Welcome to Spade’s documentation!") [Next](/battery.md "2. Charging the battery")

* * *

© Copyright 2024, Markus Simonsen, SDU.
