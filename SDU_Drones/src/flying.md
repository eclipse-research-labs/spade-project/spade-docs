3\. Flying[](#flying "Link to this heading")
=============================================

3.1. Requirements[](#requirements "Link to this heading")
----------------------------------------------------------

*   Completed previous tutorials
    
*   Installed QGroundControl
    
*   Remote controller (RC)
    

Note

QGroundControl can be downloaded from [here](https://docs.qgroundcontrol.com/en/getting_started/download_and_install.html) .

3.2. Connecting to the drone[](#connecting-to-the-drone "Link to this heading")
--------------------------------------------------------------------------------

[![_images/Disconnected_Not_Ready.png](_images/Disconnected_Not_Ready.png)](_images/Disconnected_Not_Ready.png)

To connect to the drone, you need to plug the telemetry module into your computer’s USB port. Connect your drone to your battery and open QGroundControl. Once QGroundControl has connected to your drone it should change from disconnected to Not Ready in the top left corner.

3.3. Setting up the drone[](#setting-up-the-drone "Link to this heading")
--------------------------------------------------------------------------

When opening QGroundControl you will be met by a summary page. On this summary page, you can see all of the things you should configure before taking off. Red circles indicate that you have not yet set this up, and green circles indicate tabs that are configured. Enter the tabs with red circles and follow the guides to set up your drone. However, the calibration tab should be done every time.

The different tabs will now be covered.

### 3.3.1. Firmware[](#firmware "Link to this heading")

The firmware tab is where you update the firmware of your drone. Connect your drone to your PC with a USB cable. When you enter the firmware tab. You will be met by a tab, that looks like this.

[![_images/firmware.png](_images/firmware.png)](_images/firmware.png)

Here we recommend choosing PX4 since this tutorial is based upon PX4.

### 3.3.2. Airframe[](#airframe "Link to this heading")

The airframe tab is where you choose the type of drone you are using. This is important since it will affect how the drone flies. The drone that Spade provides is a quadcopter, specifically a HolyBro QAV250.

[![_images/Airframe.png](_images/Airframe.png)](_images/Airframe.png)

### 3.3.3. Sensors[](#sensors "Link to this heading")

The next tab is the sensor tab. Here you can calibrate all of the different sensors on the drone. Just follow the guides on the screen for each of the sensors.

### 3.3.4. Radio[](#radio "Link to this heading")

The radio tab is where you calibrate your remote controller. This is important since it will affect how the drone flies. Follow the guides on the screen to calibrate your remote controller. After this notice how each of the buttons on the remote controller affects the different channels.

### 3.3.5. Flight Modes[](#flight-modes "Link to this heading")

The flight modes tab is where you set up the different flight modes, that you want to use. To read more about flight modes refer to this [link](https://docs.px4.io/main/en/getting_started/flight_modes.html). You can also set up an emergency kill switch, that will shut down the drone if you lose control of it.

We recommend that you set the channels up with the following setup if you are a beginner:

Channel 5: Flight mode

Channel 6: Kill switch

Channel 7: Arm

Channel 8: Not used

When using one switch for flight modes, it will only choose between flight modes 1, 4 and 6. We recommend that you set up the flight modes as follows:

1: Position

4: Return

6: Offboard

The position flight mode is an easy-to-use GPS-based flight mode, where the drone is controlled by the remote controller. The return flight mode will make the drone fly back to its launch position. The Offboard flight mode will enable the onboard computer to send commands to your flight controller. By using this flight mode, you can for example let the drone autonomously fly around and react to sensor inputs.

3.4. Setting up the RC[](#setting-up-the-rc "Link to this heading")
--------------------------------------------------------------------

Note

This assumes that you are using the Turnigy Evolution remote controller. If you are using a different remote controller, you should refer to the manual for that remote controller. However, the steps will most likely resemble each other.

To set up the RC turn it on and enter settings, by touching the tools icon on the main screen. You can then navigate to the AUX. channels tab. Here you can choose how to assign the two switches on the back and the button on the front. By default, the two switches work to create 9 flight modes by combining the three positions of each switch. By decoupling one of the switches, and reducing your drone to only using 3 flight modes, you can use the other switch to create an emergency kill switch. If you want to decouple the switches from choosing flight mode. You can do so from the start screen by pressing the fly mode field. This will allow you to assign the switches to other functions. We recommend that you assign the auxiliary channels as follows:

Channel 5: Sw A

Channel 6: Sw B

Channel 7: Key 1

Channel 8: None

3.5. Flying[](#id1 "Link to this heading")
-------------------------------------------

Warning

When flying drones it is important to be aware of two things. Batteries and propellers. If the batteries are damaged and have bumps or dents, please do not try to use them, as they can burst into flames. In relation to the propellers, always be careful when handling the drone, since moving propellers can cause serious damage. To gain the appropriate caution it can be recommended to search for drone propeller accidents or watch this [link](https://www.youtube.com/watch?v=QQoTQZcwZWE&ab_channel=DroneImpact-AalborgUniversity).

Before flying you should make sure that everything on the drone is fastened so that it will not interfere with the propellers. After this go outside and arm your drone. Try to let the drone lift a few feet off the ground. Then test that the kill switch properly shuts it down.

Note

If one propeller is moving in the wrong direction for take off. You need to switch two of the wires on the ESC, that are connected to the motor. This will make the motor spin in the opposite direction.

3.6. Next steps[](#next-steps "Link to this heading")
------------------------------------------------------

Now that we have the drone is flying and operatable with the RC. Let’s move on to interacting with the Raspberry Pi on the drone so that you can start implementing your own programs.

[Previous](/Battery.md "2. Charging the battery") [Next](/RaspberryPi.md "4. Interacting with the Raspberry Pi")

* * *

© Copyright 2024, Markus Simonsen, SDU.
